<?php if( get_field('notify_bool_field', 'options') ) : ?>
<div class="notify-banner">
	<div class="notify-wrapper">
		<p><?= get_field( 'notify_text_field', 'options' ); ?></p>
		<button class="notify-close-button" title="Close">&times;</button>
	</div>
</div>
<?php endif; ?>