(function($) {
    $(document).ready(function() {

		// Function to close the notification-bar
		$('.notify-close-button').click(function() {
			$('.notify-banner').hide();
		});

	}); // end doc ready
})(jQuery);