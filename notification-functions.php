<?php
/**
 * Plugin Name: Notify
 * Description: This plugin utilizes ACF to add a notification banner to the top of your site.
 * Requires at least: 5.2
 * Requires PHP: 7.2
 * Version: 1.0.1
 * Author: Shane Schroll | <a href="https://weare502.com">502</a>
*/

// Make sure ACF is installed
if( ! function_exists('the_field') && is_admin() ) {
	add_action( 'admin_notices', 'notify_error_notices' );
} else if( function_exists('acf_add_options_page') ) {

	// add an options page for the notification
	acf_add_options_page([
		'page_title' => 'Notification Banner Settings',
		'menu_title' => 'Notify',
		'menu_slug' => 'acf-options-notify',
		'capability' => 'edit_posts',
		'redirect' => false,
		'position' => '3',
		'icon_url' => 'dashicons-flag',
		'updated_message' => 'Notification Updated'
	]);

	// build out field groups - fields: textarea, true/flase
	// these fields will not show up in the Custom Fields panel and can only be edited code-side
	acf_add_local_field_group([
		'key' => 'notify_group',
		'title' => 'Notify Settings',
		'fields' => [
			[
				'key' => 'notify_text_field',
				'label' => 'Notification Text',
				'name' => 'notify_text',
				'type' => 'textarea',
				'maxlength' => '170',
				'rows' => '3'
			],

			[
				'key' => 'notify_bool_field',
				'label' => 'Enable / Disable Notification',
				'name' => 'is_notification_active',
				'type' => 'true_false',
				'ui' => 1,
				'ui_on_text' => 'Enabled',
				'ui_off_text' => 'Disabled'
			]
		],
		'location' => [[[
			'param' => 'options_page',
			'operator' => '==',
			'value' => 'acf-options-notify'
		]]],
	]);
} else {
	return; // exit no error
}

// error message if ACF is not installed (soft)
function notify_error_notices() {
	$message = "Error: This plugin requires 'Advanced Custom Fields'. Please install ACF and retry.";
	echo '<div class="error notice"><p>' . $message . '</p></div>';
}

// load styles and javascript for event handling
function notify_enqueue_scripts() {
	$plugin_url = plugin_dir_url( __FILE__ );
	$version = '1.0';
	wp_enqueue_style( 'notify-styles', $plugin_url . 'style-dist.css', [], $version );
	wp_enqueue_script( 'notify-js', $plugin_url . 'scripts/notify-dist.js', ['jquery'], $version );
}
add_action( 'wp_enqueue_scripts', 'notify_enqueue_scripts' );

function load_notify() {
	$plugin_url = plugin_dir_url( __FILE__ );
	include( 'notification-banner.php' );
}
add_action( 'wp_head', 'load_notify' );